package collections

import (
	"testing"
)

func TestNewSet(t *testing.T) {
	inputs := []int{0, 0, 1, 1, 2, 3, 4, 5, 6, 10}
	wants := []int{0, 1, 2, 3, 4, 5, 6, 10}
	s := NewSet[int](inputs...)

	if s.Size() != len(wants) {
		t.Errorf(
			"The set should have %d elements, however it contains %d elements\nset: %v\n",
			len(wants),
			s.Size(),
		)
	}

	for _, w := range wants {
		if obj, ok := s.inner[w]; !ok {
			t.Errorf("The set does not contain %v\n", obj)
		}
	}

	s = NewSet[int]()
	if s.Size() != 0 {
		t.Errorf("The set should be empty, however it contains %d elements\n", s.Size())
	}

	s = NewSet[int](inputs...)
	for _, i := range inputs {
		if !s.Contains(i) {
			t.Errorf("The set do not contains %d\n s: %v", i, s)
		}
	}
}

func TestSetContains(t *testing.T) {
	inputs := []int{0, 0, 1, 1, 2, 3, 4, 5, 6, 10}
	wants := []int{0, 1, 2, 3, 4, 5, 6, 10}
	noWants := []int{11, -1, 20, 70, 7, 8, 9}
	s := NewSet[int](inputs...)

	for _, w := range wants {
		if !s.Contains(w) {
			t.Errorf("The set does not contain %v\n", w)
		}
	}

	for _, nw := range noWants {
		if s.Contains(nw) {
			t.Errorf("The set Contains %v, while it shouldn't\n", nw)
		}
	}

	s = NewSet[int]()
	if s.Size() != 0 {
		t.Errorf("The set should be empty, however it contains %d elements\n", s.Size())
	}
}

func TestSetInsert(t *testing.T) {
	inputs := []int{0, 1, 2, 3, 4, 5, 6, 10}
	s := NewSet[int]()
	for i, obj := range inputs {
		err := s.Insert(obj)
		if err != nil {
			t.Errorf("Error in inserting %v\n", err)
		}

		if s.Size() != i+1 {
			t.Errorf("Wrong length %d, while it should be %d\n", s.Size(), i+1)
		}

		if !s.Contains(obj) {
			t.Errorf("The set should contain %v\n", obj)
		}

	}

	s = NewSet[int]()
	inputs = []int{0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
	for i, obj := range inputs {
		err := s.Insert(obj)
		if i == 0 {
			if err != nil {
				t.Errorf("Error in inserting %v\n", err)
			}
		} else {
			if err == nil {
				t.Errorf("The element %v should already be present\n", obj)
			}
		}

		if s.Size() != 1 {
			t.Errorf("Wrong length %d, while it should be %d\n", s.Size(), 1)
		}

		if !s.Contains(inputs[0]) {
			t.Errorf("The set should contain %v\n", obj)
		}
	}
}

func TestSetEmptyInsert(t *testing.T) {
	inputs := []int{0, 1, 2, 3, 4, 5, 6, 10}
	s := Set[int]{}
	for i, obj := range inputs {
		err := s.Insert(obj)
		if err != nil {
			t.Errorf("Error in inserting %v\n", err)
		}

		if s.Size() != i+1 {
			t.Errorf("Wrong length %d, while it should be %d\n", s.Size(), i+1)
		}

		if !s.Contains(obj) {
			t.Errorf("The set should contain %v\n", obj)
		}
	}
}

func TestSetPop(t *testing.T) {
	inputs := []int{0, 1, 2, 3, 4, 5, 6, 10}
	pops := []int{1, 2, 4, 5, 10}
	s := NewSet[int](inputs...)
	old_len := s.Size()
	for i, obj := range pops {
		obj2, err := s.Pop(obj)
		if err != nil {
			t.Errorf("Error in deleting %v\n", err)
		}
		if s.Contains(obj) {
			t.Errorf("Failed in deleting.")
		}

		if obj != obj2 {
			t.Errorf("Returned wrong value %v while it should be %v.", obj2, obj)
		}

		if new_len := old_len - (i + 1); s.Size() != new_len {
			t.Errorf("Wrong length %d, while it should be %d\n.%v", s.Size(), new_len, s)
		}
	}

	s = NewSet[int]()
	pops = []int{11, 20, 40, -5, -1}
	s = NewSet[int](inputs...)
	for _, obj := range pops {
		obj2, err := s.Pop(obj)
		if obj2 != 0 {
			t.Errorf(
				"Error in deleting %v, there shouldn't be any deletion\ns: %v\n", obj, s)
		}
		if err == nil {
			t.Errorf("Error in deleting %v, there shouldn't be any deletion\n", err)
		}
		if s.Contains(obj) {
			t.Errorf("The element shouldn't be present %v", obj)
		}

		if s.Size() != old_len {
			t.Errorf("Wrong length %d, while it should be %d\n.%v", s.Size(), old_len, s)
		}
	}
}
