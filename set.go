package collections

import (
	"fmt"
	"strings"
)

// type Set[T comparable] map[T]struct{}
type Set[T Element] struct {
	inner map[T]struct{}
}

func NewSet[T Element](a ...T) *Set[T] {
	set := new(Set[T])
	if len(a) == 0 {
		set.inner = make(map[T]struct{}, 10)
	} else {
		set.inner = make(map[T]struct{}, len(a))
		set.InsertAll(a...)
	}
	return set
}

func NewEmptySet[T Element](s int) *Set[T] {
	if s <= 0 {
		return nil
	}
	set := new(Set[T])
	set.inner = make(map[T]struct{}, s)
	return set
}

func CopySet[T Element](s Set[T]) (*Set[T], error) {
	if s.inner == nil {
		return nil, fmt.Errorf("Error! The set is not initialized!")
	}

	if len(s.inner) == 0 {
		return NewSet[T](), nil
	}

	if slice, err := s.Slice(); err == nil {
		return NewSet(slice...), nil
	} else {
		return nil, fmt.Errorf("Cannot slice, hence no new state")
	}
}

func (s Set[T]) Size() int {
	return len(s.inner)
}

func (s Set[T]) Contains(o T) bool {
	_, hasObj := s.inner[o]
	return hasObj
}

func (s *Set[T]) Insert(o T) error {
	if s == nil {
		return fmt.Errorf("Error! The set is not initialized!")
	}
	if !s.Contains(o) {
		s.inner[o] = struct{}{}
		return nil
	}
	return fmt.Errorf("The object %v is already present in the set.\n", o)
}

func (s1 *Set[T]) Union(s2 Set[T]) error {
	if s1 == nil || s2.inner == nil {
		return fmt.Errorf("Error! One of the sets is not initialized!")
	}
	for ele := range s2.inner {
		s1.Insert(ele)
	}
	return nil
}

func (s *Set[T]) InsertAll(a ...T) error {
	if s == nil {
		return fmt.Errorf("Error! The set is not initialized!")
	}
	for _, o := range a {
		if err := s.Insert(o); err != nil {
			return fmt.Errorf("Error! Inserting %v to %v,\n especially the element %v", a, s, o)
		}
	}
	return nil
}

func (s *Set[T]) Pop(o T) (T, error) {
	var empty T
	if s == nil {
		return empty, fmt.Errorf("Error! The set is not initialized!")
	}

	if s.Size() == 0 {
		return empty, fmt.Errorf("Error! Set empty, cannot pop!")
	}

	if s.Contains(o) {
		delete(s.inner, o)
		return o, nil
	}
	return empty, fmt.Errorf("The object %v is not in the set.\n", o)
}

func (s Set[T]) Slice() ([]T, error) {
	if s.inner == nil {
		return []T{}, fmt.Errorf("Cannot generate the slice, the set is not initialized!")
	}
	a := make([]T, s.Size())
	i := 0
	for e := range s.inner {
		a[i] = e
		i++
	}
	return a, nil
}

func (s Set[T]) MustSlice() []T {
	if s.inner == nil {
		panic("Cannot slice")
	}
	a := make([]T, s.Size())
	i := 0
	for e := range s.inner {
		a[i] = e
		i++
	}
	return a
}

func (s Set[T]) String() string {
	strs := make([]string, 0, s.Size())
	for ele := range s.inner {
		strs = append(strs, ele.String())
	}
	res := strings.Join(strs, ", ")
	return fmt.Sprintf("[%s]", res)
}
