package collections

type Element interface {
	comparable
	String() string
}

type Modifier[T any] interface {
	Insert(o *T) error
	Pop(o *T) (T, error)
}

type Shower[T Element] interface {
	Contains(o T) bool
	MustSlice() []T
	Slice() []T
	Size() int
	String() string
}

type Collection[T Element] interface {
	Modifier[T]
	Shower[T]
}
